extends Node2D

export (AudioStreamSample) var sfx

func _ready():
	$sfx.stream=sfx
	$use_area.connect("area_entered",self,"area_detected")

func usar(dir):
	show()
	$anim.frame=0
	$anims.play(dir)
#	get_node("use_area/"+dir).disabled=false
	$sfx.play()
	
	yield($anims,"animation_finished")
#	get_node("use_area/"+dir).disabled=true
	hide()

func area_detected(area):
	if area.is_in_group("enemigos"):
		area.hit_move(get_parent().direccion*2)
