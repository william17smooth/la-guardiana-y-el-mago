extends Character

#estados
const TO_IDLE = "to_idle"
const IDLE = "idle_"
const WALK = "walk_"
const USE = "use_"

export (float) var duracion_de_ataque = 0.1
export (float) var frecuencia_de_ataque = 0.2

export (NodePath) var debug

var state_machine : AnimationNodeStateMachinePlayback

var estado = IDLE
var prox_dir : Vector2


func _ready():
	state_machine = $AnimationTree.get("parameters/playback")
	state_machine.start("idle_"+str_dir(direccion_inicial))

func _physics_process(delta):
	
	######################
	get_node(debug).get_node("estado").text=estado
	######################
	
	if Input.is_action_just_pressed("ui_accept") and estado!=USE:
		estado = USE
		$item.usar(str_dir(direccion))
		state_machine.travel("use_"+str_dir(direccion))
				
	match estado:
		IDLE:
				
			if Input.is_action_pressed("abajo"):
				walk(DOWN)
				
			if Input.is_action_pressed("izquierda"):
				walk(LEFT)
				
			if Input.is_action_pressed("derecha"):
				walk(RIGHT)
				
			if Input.is_action_pressed("arriba"):
				walk(UP)
		WALK:
			walk(prox_dir)
			
			
			if Input.is_action_just_released("abajo"):
				estado = IDLE
				state_machine.travel("idle_down")
			elif Input.is_action_just_released("izquierda"):
				estado = IDLE
				state_machine.travel("idle_left")
			elif Input.is_action_just_released("derecha"):
				estado = IDLE
				state_machine.travel("idle_right")
			elif Input.is_action_just_released("arriba"):
				estado = IDLE
				state_machine.travel("idle_up")
			
		USE:
			
			if state_machine.get_current_node().begins_with(IDLE):
				estado = IDLE




func str_dir(dir):
	match dir:
		
		UP : return "up"
		DOWN : return "down"
		LEFT : return "left"
		RIGHT : return "right"
		0 : return "up"
		1 : return "down"
		2 : return "left"
		3 : return "right"

func walk(dir):
	prox_dir = dir
	if collided(dir):
		estado = IDLE
		state_machine.travel(estado + str_dir(dir))
		return
	direccion=prox_dir
	estado = WALK
	
	state_machine.travel(estado + str_dir(dir))
	move(dir)

func collided(dir):
	
	if get_node("collision_detect/"+str_dir(dir)).is_colliding() or get_node("collision_detect/"+str_dir(dir)+"2").is_colliding():
		if idle:
			direccion=prox_dir
		return true
	return false

func hit(hp):
	$sfx/hit.play()
	$anims.play("hit")
