extends Character


func _ready():
	connect("area_entered",self,"area_detected")


func area_detected(area):
	
	if area.is_in_group("player"):
		
		var dir = area.direccion*-1
		
		if area.collided(dir):
			area.hit_move(Vector2())
		else:
			area.hit_move(dir)

		area.hit(0)
