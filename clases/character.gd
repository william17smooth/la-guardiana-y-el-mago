extends Area2D

class_name Character

const UP = Vector2.UP
const DOWN = Vector2.DOWN
const LEFT = Vector2.LEFT
const RIGHT = Vector2.RIGHT

export (int) var paso = 8
export (float) var tiempo = 1.0
export (int,"UP","DOWN","LEFT","RIGHT") var direccion_inicial  



var direccion = DOWN
var idle = true

var tween = Tween.new()
var hit_tween = Tween.new()

func _ready():
	match direccion_inicial:
		0 : direccion = UP
		1 : direccion = DOWN
		2 : direccion = LEFT
		3 : direccion = RIGHT
	
	tween.playback_process_mode=Tween.TWEEN_PROCESS_PHYSICS
	hit_tween.playback_process_mode=Tween.TWEEN_PROCESS_PHYSICS
	add_child(tween)
	add_child(hit_tween)

func move (dir:Vector2):
	if idle:
		direccion = dir
		tween.interpolate_property(self, "position",
				position, position+dir*paso, tiempo,
				Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		tween.start()
		idle = false
		yield(tween,"tween_completed")
		idle=true
		

func hit_move(dir):
	
	var pos=Vector2(round(position.x/paso),round(position.y/paso))*paso
	
	hit_tween.interpolate_property(self, "position",
			position, pos+dir*paso*2, tiempo*2,
			Tween.TRANS_SINE, Tween.EASE_IN_OUT)
	hit_tween.start()
	yield(hit_tween,"tween_completed")
	print(position)

func hit(hp):
	pass
